<?php defined('SYSPATH') OR die('No direct access allowed.');


if (Kohana::$environment == Kohana::PRODUCTION)
{
    return array();
}
else if (Kohana::$environment == Kohana::STAGING)
{
    return array();
}
else
{
    return array(
        'AWS_ACCESS_KEY_ID' => '',
        'AWS_SECRET_KEY'    => '',
        'buckets' => array(
            'media'    => array( 'Bucket' => '' ),
            'statics'  => array( 'Bucket' => '',
                                 'Keys_default' => array( 'ACL' => 'public-read' )
                                ),
            'download' => array( 'Bucket' => '',
                                 'Keys_default' => array( 'StorageClass' => 'REDUCED_REDUNDANCY' ),
                                 'Lifecycle' => array(
                                        array(
                                            'ID' => 'Auto delete',
                                            'Status' => 'Enabled',
                                            'Prefix' => 'prefix_',
                                            'Expiration' => array(
                                                'Days' => '7',
                                            ),
                                        ),
                                    )
                                )
            )
    );
}
